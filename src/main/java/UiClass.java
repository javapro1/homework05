import java.util.Scanner;

public class UiClass {
    Scanner input = new Scanner(System.in);

    public String inputStringTask01() {
        System.out.println("-----------------TASK 1----------------");
        System.out.println("Enter string for first task: ");
        String stringTask = input.nextLine();
        return stringTask;
    }

    public char inputSymbolTask01() {
        System.out.println("Enter symbol for first task: ");
        String charTask = input.nextLine();
        char a = charTask.charAt(charTask.length() - 1);
        return a;
    }

    public void outResultTask01(int res1, char a, String stringTask) {
        System.out.println("There are " + res1 + " symbols " + "\"" + a + "\"" + " in the string: " + stringTask);
    }

    public String inputSourceTask02() {
        System.out.println("----------------TASK 2------------------");
        System.out.println("Input source: ");
        String source = input.nextLine();
        return source;
    }

    public String inputTargetTask02() {
        System.out.println("Input target: ");
        String target = input.nextLine();
        return target;
    }

    public void outResultTask02(int res2) {
        System.out.println("Your target word start from: " + (res2 + 1) + " position");
    }

    public String inputStringTask03() {
        System.out.println("-----------------------TASK 3-----------------------");
        System.out.println("Input string for reverse: ");
        String forReverse = input.nextLine();
        return forReverse;
    }

    public void outResultTask03(String resultOfReverse) {
        System.out.println("Result of reverse: " + resultOfReverse);
    }

    public String inputStringTask04() {
        System.out.println("-----------------------TASK 4-----------------------");
        System.out.println("Input string for checking on Palindrom: ");
        String palindrom = input.nextLine();
        return palindrom;
    }

    public void outResultTask04(boolean check) {
        System.out.println(check);
    }

    public char[] inputRandomWordTask05() {
        System.out.println("-----------------------TASK 5------------------------");
        String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado",
                "broccoli", "carrot", "cherry", "garlic", "grape", "melon", "leak", "kiwi",
                "mango", "mushroom", "nut", "olive", "pea", "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};
        int n = (int) Math.floor(Math.random() * words.length);
        char[] randomWord = words[n].toCharArray();
        return randomWord;
    }

    public String inputUserWordTask05() {
        System.out.println("Please input hidden word");
        String userWord = input.nextLine();
        return userWord;
    }

    public boolean outResultTask05(int count, int randomWordLength) {
        if (count == randomWordLength) {
            System.out.println("\nBINGO!! You are finnaly guessed");
            return false;
        } else {
            System.out.println("\nYour try is wrong, you can try again");
        }
        return true;
    }
}