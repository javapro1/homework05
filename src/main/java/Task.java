public class Task {
    public int findSymbolOccurance(String stringTask, char a) {
        char[] arr = stringTask.toCharArray();
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == a) {
                count++;
            }
        }
        return count;
    }

    public int findWordPosition(String source, String target) {
        return source.indexOf(target, 0);
    }

    public String stringReverse(String forReverse) {
        char[] arr = forReverse.toCharArray();
        String result = "";
        for (int i = arr.length - 1; i >= 0; i--) {
            result = result + arr[i];
        }
        return result;
    }

    public boolean isPalindrome(String palindrom) {
        char[] symbol = palindrom.toCharArray();
        int l = symbol.length;
        for (int i = 0; i < symbol.length; i++) {
            if (!(symbol[i] == symbol[l - 1])) {
                return false;
            }
            l--;
        }
        return true;
    }

    public int crossword(char[] randomWord, char[] userAnswer) {
        int count = 0;
        for (int i = 0; i < 15; i++) {
            if (((userAnswer[i] | randomWord[i]) == '\u0000') || (userAnswer[i] != randomWord[i])) {
                System.out.print("#");
            } else if (userAnswer[i] == randomWord[i]) {
                System.out.print(userAnswer[i]);
                count++;
            }
        }
        return count;
    }
}
