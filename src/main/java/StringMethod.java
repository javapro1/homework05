import java.util.Arrays;

public class StringMethod {
    public static void main(String[] args) {
        UiClass ui = new UiClass();
        Task task = new Task();
        String stringTask = ui.inputStringTask01();
        char a = ui.inputSymbolTask01();
        int res1 = task.findSymbolOccurance(stringTask, a);
        ui.outResultTask01(res1, a, stringTask);

        String source = ui.inputSourceTask02();
        String target = ui.inputTargetTask02();
        int res2 = task.findWordPosition(source, target);
        ui.outResultTask02(res2);

        String forReverse = ui.inputStringTask03();
        String resultOfReverse = task.stringReverse(forReverse);
        ui.outResultTask03(resultOfReverse);

        String palindrom = ui.inputStringTask04();
        boolean check = task.isPalindrome(palindrom);
        ui.outResultTask04(check);

        char[] randomWord = ui.inputRandomWordTask05();
        boolean result = true;
        while (result) {
            String userWord = ui.inputUserWordTask05();
            char[] userAnswer = userWord.toCharArray();
            int userWordLength = userWord.length();
            int randomWordLength = randomWord.length;
            char[] newRandomWord = Arrays.copyOf(randomWord, 15);
            char[] newAnswerWord = Arrays.copyOf(userAnswer, 15);
            int count = task.crossword(newRandomWord, newAnswerWord);
            result = ui.outResultTask05(count, randomWordLength);
        }
    }
}
